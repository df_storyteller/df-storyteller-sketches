# DF Storyteller Sketches

Dwarf Fortress Storyteller Sketches is a list of examples that use 
[DF Storyteller](https://gitlab.com/df_storyteller/df-storyteller)
to visualize the data. These example serve as a starting point for new people to quickly get started.

For more information see the main project [DF Storyteller](https://gitlab.com/df_storyteller/df-storyteller)

DF Storyteller takes the legend files from Dwarf Fortress and DFHack and imports them into a database.
It then provides and API to view the data.

![process](./images/processbanner.png)

## Examples

### Simple List
This is a simple list of some `sites` (cities, camps, pits, ...).
This example should get you started with the basic concept of how things work.

This should be easy to understand even for beginning programmers (at least I hope so).

### Example Basic JS + Chart js
![Pie chart](./images/DF Storyteller Example Basic JS + Chart js.png)

### Timeline
![Timeline](./images/DF Storyteller Timeline.png)

## Get started
Feel free to edit the code here and make your own webpage from this.
If you have questions, please join the [Discord](https://discord.gg/aAXt6uu) and ask questions.
Even if you don't know much about programming, 
we will help you or point you in the right direction.

If you don't know much about Javascript or HTML take a look at here
https://developer.mozilla.org/en-US/docs/Web
Or when you want to find out what a function does. 
For example `.map()` function used in Javascript:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map

You can do it!

And remember: Just search for the problem you have will most likely give you an answer.
* What does fetch do in js (javascript)? Search [`js fetch`](https://duckduckgo.com/?q=js+fetch)
* What is a `<canvas>` in HTML? Search [`html canvas`](https://duckduckgo.com/?q=html+canvas)
* ...

## Want to contribute?

If you want to contribute to the examples feel free to open a merge request.
For more information and out Code of Conduct see the main project 
[DF Storyteller](https://gitlab.com/df_storyteller/df-storyteller)


## License

Licensed under either of [Apache License, Version 2.0](LICENSE-APACHE) or 
[MIT license](LICENSE-MIT) at your option.
Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in 
DF Storyteller Sketches by you, as defined in the Apache-2.0 license, shall be dual licensed as 
above, without any additional terms or conditions. 